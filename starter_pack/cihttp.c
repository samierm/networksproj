#include "cihttp.h"

void *GetInAddr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

void AddHeaderToRequest(struct httprequest *req, char *name, char *value)
{
    NVL *header = malloc(sizeof(NVL));
    header->name = name;
    header->value = value;
    if (req->headers)
        header->next = req->headers;
    else
        header->next = NULL;
    req->headers = header;
}

void AddHeaderToResponse(struct httpresponse *res, char *name, char *value)
{
    NVL *header = malloc(sizeof(NVL));
    header->name = name;
    header->value = value;
    if (res->headers)
        header->next = res->headers;
    else
        header->next = NULL;
    res->headers = header;
}

void AddHeaderToPostData(struct httprequest *req, char *name, char *value)
{
    NVL *post_data = malloc(sizeof(NVL));
    post_data->name = name;
    post_data->value = value;
    if(req->post_data)
        post_data->next = req->post_data;
    else
        post_data->next = NULL;
    req->post_data = post_data;
}

char * generatePostBody(struct httprequest *req, char *returnString){
	char *currentPostBody = (char*) malloc(strlen(req->body)+100);
	//char modifiedBody[] = "";
	char valueBoxPrefix[] ="<input type=\"text\" ";
	char replacementBuffer[80];
	
	/* strcpy(currentPostBody,req->body);
	
	char *token = strtok(currentPostBody, valueBoxPrefix);
	printf("In generatePostBody\n%s\n",token);

	strcat(modifiedBody,token);
	strcat(modifiedBody,valueBoxPrefix);
	printf("In generatePostBody\n%s\n",modifiedBody);

	token = strtok(NULL, "\"");
	strcat(modifiedBody,token);
	
	NVL *matchedPostData = findMatchingPostData(req, token);
	sprintf(replacementBuffer,"\"%s\"",matchedPostData->value);
	strcat(modifiedBody,replacementBuffer);
	
	token = strtok(currentPostBody, valueBoxPrefix);
	strcat(modifiedBody,token);
	strcat(modifiedBody,valueBoxPrefix);
	token = strtok(NULL, "\"");
	strcat(modifiedBody,token);
	
	matchedPostData = findMatchingPostData(req, token);
	sprintf(replacementBuffer,"\"%s\"",matchedPostData->value);
	strcat(modifiedBody,replacementBuffer); */

	sprintf(currentPostBody,"<html>\r\n\r\n<head>\r\n\t<title>FORM PAGE</title>\r\n</head>\r\n\r\n<body>\r\n\t<h1>Hopefully your POST works!</h1>\r\n\t<form action=\"post_test\" method=\"post\">\r\n\t\tName (first and last):\r\n\t\t<input type=\"text\" name=\"%s\">\t\t<br>\r\n\t\tCourse:\r\n\t\t<input type=\"text\" name=\"%s\">\r\n\t\t<br><br>\r\n\t\t<input type=\"submit\" value=\"Submit\">\r\n\t</form>\r\n</body>\r\n\r\n</html>",req->post_data->next->value,req->post_data->value);
	
	
	returnString = (char*) malloc(strlen(currentPostBody)+100);

	strcpy(returnString,currentPostBody);
	
	return returnString;
}

NVL * findMatchingPostData(struct httprequest *req, char *dataName){
	NVL *currentDataHeader = req->post_data;
	
	while(currentDataHeader !=NULL){
		if(strcmp(currentDataHeader->name,dataName)==0)
			return currentDataHeader;
		
		currentDataHeader = currentDataHeader->next;
	}
	
	return NULL;
}

struct httprequest *ParseHTTPRrequest(char *data)
{
    struct httprequest *req;
    req = malloc(sizeof(struct httprequest));
	char *token;
	
	
	if(strlen(data) < 1)
		return NULL;
	
	//Collect Method
	token = strtok(data," ");
	int i = 0;
	for(i=0; i < TOTAL_METHODS; i++){
		if(strcmp(token,implemented_methods[i]) == 0){
			req->method = i;
			break;
		}
	}
	
	//collect uri
	token = strtok(NULL," ");
	if(token == NULL)
		return req;
	req->request_uri = (char *) malloc(strlen(token)+1);
	strcpy(req->request_uri,token);
	
	
	//collect version
	token = strtok(NULL,"HTTP/"); //discard the HTTP/ for the float version
    if(token == NULL)
		return req;
	
	req->version = atof(token);
	
	//gather headers
	
	char *currentHeaderValue;
	
	while((token = strtok(NULL,"\r\n:")) != NULL && (currentHeaderValue = strtok(NULL,"\r\n"))!= NULL && currentHeaderValue != "\n"){
		AddHeaderToRequest(req,token,currentHeaderValue);        
	}
	
	if(req->method == POST){
		char *rawURI = (char*) malloc(strlen(req->request_uri)+1);
		char *name, *course;
		char *nameValue,*courseValue;
		strcpy(rawURI,req->request_uri);
		token = strtok(rawURI, "?");
		strcpy(req->request_uri,token);
		//given back post URI
		
		token = strtok(NULL,"=");
		name = (char*) malloc(strlen(token)+1);
		strcpy(name,token);
		token = strtok(NULL,"&");
		nameValue = (char*) malloc(strlen(token)+1);
		strcpy(nameValue,token);
		
		AddHeaderToPostData(req,name,nameValue);
		
		token = strtok(NULL,"=");
		course = (char*) malloc(strlen(token)+1);
		strcpy(course,token);
		token = strtok(NULL,"\r\n");
		courseValue = (char*) malloc(strlen(token)+1);
		strcpy(courseValue,token);
		AddHeaderToPostData(req,course,courseValue);
	}
	else{
		req->post_data = NULL;
	}
	
	size_t lineSize = MAX_LEN;
	char bodyBuffer[MAX_FILE_LEN] = "";
	char lineBuffer[lineSize];
	FILE *filePointer;
	
	char fileName[50] = "www/";
	strcat(fileName,req->request_uri);
	
	//collect body
	if((req->method == GET || req->method == POST) && (filePointer = (fopen(fileName,"r"))) ){
		while(fscanf(filePointer,"%s",lineBuffer) == 1){
			strcat(bodyBuffer,lineBuffer);
			strcat(bodyBuffer,"\r\n");
		}
		req->body = (char*)malloc(strlen(bodyBuffer)+1);
		strcpy(req->body,bodyBuffer);
		fclose(filePointer);
	}
	
    return req;
}

void PrintHTTPRequest(struct httprequest *req)
{
	printf("\nHTTP %.1f %s Request\n",req->version,implemented_methods[req->method]);
	printf("Request-URI: %s\n", req->request_uri);
	
	printf("Headers:\n");
	NVL *curr_header = req->headers;
	
	while(curr_header != NULL && curr_header->name != NULL && curr_header->value != NULL){

		printf("\t%s: %s\r\n",curr_header->name,curr_header->value);
		curr_header = curr_header->next;	
	}
	
	curr_header = req->post_data;
	printf("Post Data:\n");
	while(curr_header != NULL && curr_header->name != NULL && curr_header->value != NULL){
		printf("\t%s: %s\n", curr_header->name, curr_header->value);
        curr_header = curr_header->next;
	}
	
}

struct httpresponse *GenerateHTTPResponse(struct httprequest *req)
{
    static int PREFIX_LENGTH =5;

    FILE *filePointer = (FILE *)malloc(sizeof(FILE*));
    struct stat fileInformation;
	time_t modifiedTime;
    struct httpresponse *res = (struct httpresponse *)malloc(sizeof(struct httpresponse));
    char *fileName = (char *) malloc(PREFIX_LENGTH *sizeof(char));
	char valueBuffer[100];
	
	strcpy(fileName, "www/");
    strcat(fileName, req->request_uri);
	

    if( (filePointer=fopen(fileName,"r")) != NULL){
        stat(fileName,&fileInformation);
		
        res->status = 200;
		res->reason = (char*) malloc(strlen("OK")+1);
        strcpy(res->reason,"OK");
		
		if(req->method == GET || req->method == POST ){
			res->body = (char*)malloc(strlen(req->body)+1);
			strcpy(res->body, req->body);
			
			if(req->method == POST){
				char *modifiedPostBody[MAX_FILE_LEN]; //= generatePostBody(req,modifiedPostBody);
				sprintf(modifiedPostBody,"<html><body><h1>Name: %s</h1><h2>\nCourse: %s</h2></body></html>", req->post_data->next->value,req->post_data->value);
				res->body = (char*) malloc(strlen(modifiedPostBody)+1);
				strcpy(res->body,modifiedPostBody);
			}
		}
		else if(req->method == HEAD)
			res->body == NULL;
		
    }
    else{
        res->status = 404;
		res->reason = (char *) malloc(MAX_LEN * sizeof(char));
        strcpy(res->reason,"File Not Found\r\n");
		char page404[] = "<html>\r\n\r\n<body>/r/n/t<h1>404 Page Not Found</h1>/r/n</body?/r/n/r/n</html>/r/n";
		res->body = (char *) malloc(strlen(page404)+1);
		stpcpy(res->body,page404);
		res->headers = NULL;
		printf("Correctly genereated 404\n");
		return res;
    }

    res->version = req->version;
	
	
    sprintf(valueBuffer, "%d", (int)fileInformation.st_size);
	char* lengthBuffer = (char*) malloc(strlen(valueBuffer));
	strcpy(lengthBuffer,valueBuffer);
	modifiedTime = fileInformation.st_mtime;
	
	AddHeaderToResponse(res,"Server: ","cihttp");
	AddHeaderToResponse(res,"Content-length: ",lengthBuffer);    
    strftime(valueBuffer, 30, "%a, %d %b %Y %H:%M:%S %Z", gmtime(&modifiedTime));
	AddHeaderToResponse(res,"Last-modified: ",valueBuffer);

    return res;
}

void SendHTTPResponse(struct httpresponse *res, int cfd)
{
	//copy status line
	char responseString[MAX_LEN];
	sprintf(responseString, "HTTP/%.1f %d %s\r\n", res->version, res->status, res->reason);
	
	//status line copied
	//////////////////////////////////
	
	//copy headers
	NVL *currHeader = res->headers;
	
	while(currHeader != NULL && currHeader->name != NULL && currHeader->value != NULL){
		strcat(responseString, currHeader->name);
		strcat(responseString, currHeader->value);
		strcat(responseString, "\r\n");
		
		currHeader = currHeader->next;
	}
	strcat(responseString, "\r\n");
	
	
	
	//headers copied
	////////////////////////////////////
	
	//copy body
	if(res->body != NULL)
		strcat(responseString, res->body);
	//body copied
	////////////////////////////////////
	
	
    char testMessage[]= "HTTP/1.1 200 OK\r\nServer: cihttp\r\n\r\n"; //works in this form
    if(send(cfd,responseString,strlen(responseString),0) != -1)
		printf("\nResponse should have been sent\n");
}

int main(int argc, char *argv[])
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    struct sockaddr_storage their_addr;
    int sfd, optval, res, cfd;
    socklen_t sin_size;
    char ip[INET_ADDRSTRLEN];

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    res = getaddrinfo(NULL, PORT, &hints, &result);
    if (res != 0)
    {
        fprintf(stderr, "getaddrinfo() error: %s\n", strerror(errno));
    }

    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
        {
            fprintf(stderr, "socket() error: %s\n", strerror(errno));
            continue;
        }

        optval = 1;
        res = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
        if (res != 0)
        {
            close(sfd);
            fprintf(stderr, "setsockopt() error: %s\n", strerror(errno));
            exit(1);
        }

        res = bind(sfd, rp->ai_addr, rp->ai_addrlen);
        if (res != 0)
        {
            close(sfd);
            fprintf(stderr, "bind() error: %s\n", strerror(errno));
            continue;
        }

        break;
    }

    freeaddrinfo(result);

    if (rp == NULL)
    {
        fprintf(stderr, "server: failed to bind\n");
        exit(1);
    }

    res = listen(sfd, BACKLOG);
    if (res != 0)
    {
        fprintf(stderr, "listen() error: %s\n", strerror(errno));
        exit(1);
    }

    for (;;)
    {
        sin_size = sizeof their_addr;
        cfd = accept(sfd, (struct sockaddr *)&their_addr, &sin_size);
        if (cfd == -1)
        {
            fprintf(stderr, "accept() error: %s\n", strerror(errno));
            continue;
        }

        switch (fork())
        {
        case -1: // error
            fprintf(stderr, "fork() can't create child: %s\n", strerror(errno));
            close(cfd);
            break;
        case 0: // child
        {
            char *data = malloc(MAX_LEN * sizeof(char));
            int res;

            // child doesn't need listening socket
            close(sfd);

            inet_ntop(their_addr.ss_family, GetInAddr((struct sockaddr *)&their_addr),
                      ip, sizeof(ip));
            printf("server: got a connection from %s\n", ip);

            res = recv(cfd, data, MAX_LEN, 0);
            if (res == 0 || res == -1)
            {
                fprintf(stderr, "server: recv'd no bytes (%s)\n", strerror(errno));
                return 0;
            }

            struct httprequest *req = ParseHTTPRrequest(data);
            PrintHTTPRequest(req);
            struct httpresponse *response = GenerateHTTPResponse(req);
            SendHTTPResponse(response, cfd);

            free(req);
            free(response);
            free(data);

            exit(0);
            break;
        }
        default: // parent
            // parent doesn't need client socket
            close(cfd);
            break;
        }
    }
}